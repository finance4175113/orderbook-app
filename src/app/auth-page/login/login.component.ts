import { Component, OnInit } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { AuthService } from '../shared/service/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatSnackBarModule,
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
})
export class LoginComponent implements OnInit {
  hide = true;

  rememberMe = false;

  loginForm: FormGroup = new FormGroup({});
  constructor(
    private _fb: FormBuilder,
    private _authService: AuthService,
    private _router: Router,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.loginForm = this._fb.group({
      clientID: ['', [Validators.required, Validators.pattern(/^[a-zA-Z0-9]+$/)]],
      password: ['', [Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!$@&])(?!.*\s).{8,}$/)]],
    });
  }

  rememberMeState() {
    this.rememberMe = !this.rememberMe;
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this._authService.loginUser(this.loginForm.value).subscribe({
        next: (response) => {
          this.showMessage(response.message);
          if (this.rememberMe === false) {
            sessionStorage.setItem('Token', response.Token);
          } else {
            localStorage.setItem('Token', response.Token);
          }
          this._router.navigate(['dashboard']);
        },
        error: (error) => {
          this.showMessage(error.error.message);
        },
      });
    }
  }

  showMessage(message: string) {
    this._snackBar.open(message, 'Close', {
      duration: 10000,
    });
  }
}
