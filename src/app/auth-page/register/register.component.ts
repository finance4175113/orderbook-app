import { DialogRef } from '@angular/cdk/dialog';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { AuthService } from '../shared/service/auth.service';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatSnackBarModule,
  ],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css',
})
export class RegisterComponent implements OnInit {
  hide = true;

  constructor(
    public dialogRef: DialogRef,
    private _fb: FormBuilder,
    private _authService: AuthService,
    private _snackBar: MatSnackBar
  ) { }

  registerForm: FormGroup = new FormGroup({});

  ngOnInit(): void {
    this.registerForm = this._fb.group({
      first_name: ['', [Validators.required, Validators.pattern(/^[a-zA-Z]+$/)]],
      last_name: ['', [Validators.required, Validators.pattern(/^[a-zA-Z]+$/)]],
      clientID: ['', [Validators.required, Validators.pattern(/^[a-zA-Z0-9]+$/)]],
      password: ['', [Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!$@&])(?!.*\s).{8,}$/)]],
    });
  }

  onSubmit() {
    if (this.registerForm.valid) {
      this._authService.createUser(this.registerForm.value).subscribe({
        next: (response) => {
          this.showMessage(response.message);
          this.dialogRef.close();
        },
        error: (error) => {
          this.showMessage(error.error.message);
        },
      });
    }
  }

  showMessage(message: string) {
    this._snackBar.open(message, 'Close', {
      duration: 10000,
    });
  }
}
