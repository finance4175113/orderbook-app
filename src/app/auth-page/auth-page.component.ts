import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { Dialog, DialogModule } from '@angular/cdk/dialog';
import { RegisterComponent } from './register/register.component';

@Component({
  selector: 'app-auth-page',
  standalone: true,
  imports: [RouterOutlet, MatButtonModule, DialogModule],
  templateUrl: './auth-page.component.html',
  styleUrl: './auth-page.component.css',
})
export class AuthPageComponent {
  hide = true;

  constructor(private _dialog: Dialog) {}

  openRegisterPage() {
    this._dialog.open(RegisterComponent, {
      disableClose: true,
      autoFocus: false,
    });
  }
}
