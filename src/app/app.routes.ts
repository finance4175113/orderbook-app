import { Routes } from '@angular/router';
import { authGuard } from './auth-page/shared/service/auth.guard';

export const routes: Routes = [
    { path: "", redirectTo: "dashboard", pathMatch: "full" },
    { path: "dashboard", loadComponent: () => import('./dashboard-page/dashboard-page.component').then((m) => m.DashboardPageComponent), canActivate: [authGuard] },
    {
        path: "", loadComponent: () => import('./auth-page/auth-page.component').then((m) => m.AuthPageComponent), children: [
            { path: "login", loadComponent: () => import('./auth-page/login/login.component').then((m) => m.LoginComponent) }
        ]
    },
    { path: "**", loadComponent: () => import('./page-not-found-page/page-not-found-page.component').then((m) => m.PageNotFoundPageComponent) }
];
