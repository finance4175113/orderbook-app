import { Component } from '@angular/core';

@Component({
  selector: 'app-page-not-found-page',
  standalone: true,
  imports: [],
  templateUrl: './page-not-found-page.component.html',
  styleUrl: './page-not-found-page.component.css'
})
export class PageNotFoundPageComponent {

}
