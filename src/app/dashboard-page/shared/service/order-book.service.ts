import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, interval, switchMap, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderBookService {

  private _url = "http://127.0.0.1:3000/api/v1/limitOrder"
  constructor(private _http: HttpClient) { }

  getAll(): Observable<any> {
    return interval(1250).pipe(
      switchMap(() => this._http.get<any>(`${this._url}/all`)),
      catchError((error: HttpErrorResponse) => {
        return throwError(() => new Error(error.error.message))
      })
    );
  }

  createOrder(order: any): Observable<any> {
    return this._http.post<any>(`${this._url}/create`, order)
  }

  getClientOrders(): Observable<any> {
    return this._http.get<any>(`${this._url}/clientOrders`);
  }

  cancelOrder(id: string): Observable<any> {
    return this._http.delete<any>(`${this._url}/cancel/${id}`)
  }

}
