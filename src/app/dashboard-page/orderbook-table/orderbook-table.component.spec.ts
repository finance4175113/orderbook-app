import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderbookTableComponent } from './orderbook-table.component';

describe('OrderbookTableComponent', () => {
  let component: OrderbookTableComponent;
  let fixture: ComponentFixture<OrderbookTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OrderbookTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(OrderbookTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
