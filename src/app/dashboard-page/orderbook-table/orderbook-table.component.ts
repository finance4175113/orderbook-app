import { Component, OnInit } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { OrderBookService } from '../shared/service/order-book.service';

@Component({
  selector: 'app-orderbook-table',
  standalone: true,
  imports: [MatTableModule],
  templateUrl: './orderbook-table.component.html',
  styleUrl: './orderbook-table.component.css',
})
export class OrderbookTableComponent implements OnInit {
  bidDisplayedColumns: string[] = ['bidVolume', 'bid'];
  askDisplayedColumns: string[] = ['ask', 'askVolume'];

  bidDataSource: any[] = [];
  askDataSource: any[] = [];

  constructor(private _orderBookService: OrderBookService) {}

  ngOnInit(): void {
    this._orderBookService.getAll().subscribe({
      next: (response) => {
        this.bidDataSource = response.orders.map((item: any) => item.bid)[0];
        this.askDataSource = response.orders.map((item: any) => item.ask)[0];
      },
      error: (error) => {
        console.log(error.message);
      },
    });
  }
}
