import { Component } from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';
import { BidFormComponent } from './bid-form/bid-form.component';
import { AskFormComponent } from './ask-form/ask-form.component';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { OrderbookTableComponent } from './orderbook-table/orderbook-table.component';
import { ClientTransactionHistoryComponent } from './client-transaction-history/client-transaction-history.component';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';

@Component({
  selector: 'app-dashboard-page',
  standalone: true,
  imports: [
    MatTabsModule,
    BidFormComponent,
    AskFormComponent,
    MatButtonModule,
    MatSnackBarModule,
    OrderbookTableComponent,
    ClientTransactionHistoryComponent,
    MatDividerModule,
    MatCardModule
  ],
  templateUrl: './dashboard-page.component.html',
  styleUrl: './dashboard-page.component.css',
})
export class DashboardPageComponent {
  constructor(private _router: Router) { }

  logout() {
    if (localStorage.getItem('Token')) {
      localStorage.removeItem('Token');
    } else {
      sessionStorage.removeItem('Token');
    }
    this._router.navigate(['login']);
  }
}
