import { Component, OnInit } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { OrderBookService } from '../shared/service/order-book.service';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-client-transaction-history',
  standalone: true,
  imports: [MatTableModule, MatButtonModule],
  templateUrl: './client-transaction-history.component.html',
  styleUrl: './client-transaction-history.component.css'
})
export class ClientTransactionHistoryComponent implements OnInit {
  dataSource = [];
  displayedColumns: string[] = ['position', 'price', 'volume', 'side', 'status', 'cancel'];

  constructor(private _orderbookService: OrderBookService, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.fetchData();
  }

  fetchData() {
    this._orderbookService.getClientOrders().subscribe({
      next: (response) => {
        this.dataSource = response;
      },
      error: (error) => {
        console.log(error);
      }
    })
  }

  cancelOrder(id: string) {
    this._orderbookService.cancelOrder(id).subscribe({
      next: (response) => {
        this.showMessage(response.message)
        this.fetchData()
      },
      error: (error) => {
        this.showMessage(error.error.message)
      }
    })
  }

  showMessage(message: string) {
    this._snackBar.open(message, 'Close', {
      duration: 10000,
    });
  }

}
