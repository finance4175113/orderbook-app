import { Component, EventEmitter, Output } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { OrderBookService } from '../shared/service/order-book.service';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-bid-form',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatSnackBarModule,
  ],
  templateUrl: './bid-form.component.html',
  styleUrl: './bid-form.component.css',
})
export class BidFormComponent {
  @Output() updateData = new EventEmitter<void>();
  bidForm: FormGroup = new FormGroup({});
  suffix: string = '.00';
  constructor(
    private _fb: FormBuilder,
    private _orderBookService: OrderBookService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.bidForm = this._fb.group({
      side: ['Bid', [Validators.required]],
      price: ['', [Validators.required, Validators.pattern(/^\d+(?:\.\d{1,2})?$/)]],
      volume: ['', [Validators.required, Validators.pattern(/^\d+$/)]]
    });

    this.bidForm.get('price')?.valueChanges.subscribe({
      next: (response) => {
        this.suffix = response && response.toString().includes('.') ? '' : '.00'
      }
    })
  }

  onSubmit() {
    if (this.bidForm.valid) {
      this._orderBookService.createOrder(this.bidForm.value).subscribe({
        next: (response) => {
          this.showMessage(response.message);
          this.updateData.emit();
        },
        error: (error) => {
          this.showMessage(error.error.message);
        },
      });
    }
  }

  showMessage(message: string) {
    this._snackBar.open(message, 'Close', {
      duration: 10000,
    });
  }
}
